from trytond.pool import Pool
from .uom import UOM


def register():
    Pool.register(
        UOM,
        module='product_uom_cat_dose', type_='model')
