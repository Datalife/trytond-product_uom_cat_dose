# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool


class CategoryDoseTestCase(ModuleTestCase):
    """Test dose product category module"""
    module = 'product_uom_cat_dose'

    @with_transaction()
    def test_uom_compute_dose_qty(self):
        'Test uom compute_dose_qty function'
        pool = Pool()
        Uom = pool.get('product.uom')

        # dose_quantity, factor, _dividend_name, factor_uom_name,
        # to_uom_name, result
        tests = [
            (15, 100, 'Kilogram per Hectare', 'Hectare', None, 1500),
            (10, 5, 'Liter per Hectare', 'Hectare', None, 50),
            (15, 100, 'Kilogram per Hectare', 'Hectare', 'Gram', 1500000),
            (200, 3000, 'Cubic centimeter per Liter', 'Cubic meter',
                'Liter', 0.6)
        ]

        for (dose_quantity, factor, _dividend_name, factor_uom_name,
                to_uom_name, result) in tests:
            _dividend, = Uom.search([
                    ('name', '=', _dividend_name),
                    ], limit=1)
            factor_uom, = Uom.search([
                    ('name', '=', factor_uom_name),
                    ], limit=1)

            to_uom = None
            if to_uom_name:
                to_uom, = Uom.search([
                        ('name', '=', to_uom_name),
                        ], limit=1)

            result2 = _dividend.compute_dose_qty(dose_quantity=dose_quantity,
                factor=factor, factor_uom=factor_uom, to_uom=to_uom)

            self.assertEqual(result, result2)

    @with_transaction()
    def test_uom_compute_dose(self):
        'Test uom compute_dose_qty function'
        pool = Pool()
        Uom = pool.get('product.uom')

        # quantity, factor, _dividend_name, factor_uom_name,
        # to_uom_name, result
        tests = [
            (1500, 100, 'Kilogram', 'Kilogram per Hectare', 15),
            (50, 5, 'Liter', 'Liter per Hectare', 10),
            (1500000, 100, 'Gram', 'Kilogram per Hectare', 15),
            (100, 1, 'Liter', 'Kilogram per Hectare', 100),
        ]

        for (quantity, factor, _dividend_name, factor_uom_name,
                result) in tests:
            _dividend, = Uom.search([
                    ('name', '=', _dividend_name),
                    ], limit=1)
            factor_uom, = Uom.search([
                    ('name', '=', factor_uom_name),
                    ], limit=1)

            result2 = _dividend.compute_dose(quantity, factor, factor_uom)

            self.assertEqual(result, result2)

    @with_transaction()
    def test_dose_unit(self):
        'Test getting dose unit for a given uom'
        pool = Pool()
        Uom = pool.get('product.uom')

        tests = [
            ('Kilogram', 'Hectare', 'Kilogram per Hectare'),
            ('Liter', 'Hectare', 'Liter per Hectare'),
            ('Gram', 'Hectare', 'Kilogram per Hectare'),
            ('Gram', 'Liter', 'Gram per Liter'),
            ('Liter', 'Liter', 'Cubic centimeter per Liter'),
        ]

        for qty_uom, factor_uom, dose_uom in tests:
            qty_uom, = Uom.search([
                    ('name', '=', qty_uom),
                ], limit=1)
            factor_uom, = Uom.search([
                    ('name', '=', factor_uom),
                ], limit=1)

            result = qty_uom._get_dose_unit(factor_uom)

            self.assertEqual(result.name, dose_uom)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        CategoryDoseTestCase))
    return suite
