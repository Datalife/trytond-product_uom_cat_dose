# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool

__all__ = ['UOM']


class UOM(metaclass=PoolMeta):
    __name__ = 'product.uom'

    def _get_dose_uoms(self):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        if not self.category.id == Modeldata.get_id('product_uom_cat_dose',
                'uom_cat_dose'):
            return None
        _divisor = self._get_dose_divisor_unit()
        _dividend = self._get_dose_dividend_unit()
        return _dividend, _divisor

    def _get_dose_divisor_unit(self):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Uom = pool.get('product.uom')
        if self.id in [Modeldata.get_id('product_uom_cat_dose', 'uom_kg_ha'),
                       Modeldata.get_id('product_uom_cat_dose', 'uom_l_ha')]:
            return Uom(Modeldata.get_id('product', 'uom_hectare'))
        if self.id in [Modeldata.get_id('product_uom_cat_dose', 'uom_gr_l'),
                       Modeldata.get_id('product_uom_cat_dose', 'uom_cm3_l')]:
            return Uom(Modeldata.get_id('product', 'uom_liter'))
        return None

    def _get_dose_dividend_unit(self):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Uom = pool.get('product.uom')
        if self.id == Modeldata.get_id('product_uom_cat_dose', 'uom_kg_ha'):
            return Uom(Modeldata.get_id('product', 'uom_kilogram'))
        if self.id == Modeldata.get_id('product_uom_cat_dose', 'uom_l_ha'):
            return Uom(Modeldata.get_id('product', 'uom_liter'))
        if self.id == Modeldata.get_id('product_uom_cat_dose', 'uom_gr_l'):
            return Uom(Modeldata.get_id('product', 'uom_gram'))
        if self.id == Modeldata.get_id('product_uom_cat_dose', 'uom_cm3_l'):
            return Uom(Modeldata.get_id('product', 'uom_cubic_centimeter'))
        return None

    def _get_dose_unit(self, divisor_uom):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Uom = pool.get('product.uom')

        if not divisor_uom:
            return None

        def get_category(name):
            return Modeldata.get_id('product', 'uom_cat_%s' % name)

        category_id = self.category.id
        if divisor_uom.category.id == get_category('surface'):
            if category_id == get_category('weight'):
                return Uom(Modeldata.get_id(
                    'product_uom_cat_dose', 'uom_kg_ha'))
            if category_id == get_category('volume'):
                return Uom(Modeldata.get_id(
                    'product_uom_cat_dose', 'uom_l_ha'))
        if divisor_uom.category.id == get_category('volume'):
            if category_id == get_category('weight'):
                return Uom(Modeldata.get_id(
                    'product_uom_cat_dose', 'uom_gr_l'))
            if category_id == get_category('volume'):
                return Uom(Modeldata.get_id(
                    'product_uom_cat_dose', 'uom_cm3_l'))
        return None

    def compute_dose_qty(self, dose_quantity, factor, factor_uom, to_uom=None):
        pool = Pool()
        Uom = pool.get('product.uom')

        _dividend = self._get_dose_dividend_unit()
        if to_uom and to_uom.category.id != _dividend.category.id:
            return dose_quantity

        qty = factor_uom.round(
            dose_quantity * Uom.compute_qty(self._get_dose_divisor_unit(),
            factor, factor_uom))

        if to_uom and _dividend.id != to_uom.id:
            qty = Uom.compute_qty(_dividend, qty, to_uom)
        return qty

    def compute_dose(self, quantity, factor, dose_uom):
        """Compute dose for a given quantity and dose unit"""
        """ self is quantity unit"""
        _dividend = dose_uom._get_dose_dividend_unit()
        if not _dividend:
            return quantity
        if self.category.id != _dividend.category.id:
            return quantity

        if self.id != _dividend.id:
            quantity = self.compute_qty(self, quantity, _dividend)

        return dose_uom.round(quantity / factor)
